# Easily Building Web Crawlers for Data Gathering

## Abstract
For a recent work project, I determined one of the easiest ways to get data from 20k+ pages was to build a web crawler 
and scrape data. I ended up using Scrapy, a Python framework, to build some super simple spiders/crawlers to traverse 
the sites to get a lot of data we needed. I can show you how to use it, why you might, and example code.

## Version History:
Date | Version | Venue
---- | ------- | -----
2021-12-20 | 1.0 | Code & Supply Pittsburgh Meetup Dec 2021 Lightning Talk Night

# Run slides

Easy Method:
You can load `index.html` in the browser

Full-Featured Method:
1. Run `npm install`
2. Run 'npm start'
3. Go to `https://localhost:8000`
4. Press `F` to go full-screen
5. Press `S` to open up speaker view

## Contact
I'd love to answer any questions you have about my session. The easiest method to contact me is by Twitter: 
[@geekygirlsarah](https://www.twitter.com/geekygirlsarah). Other contact info is on my last slide.